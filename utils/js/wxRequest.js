
const wxPromise = require('./wxPromise.js');
function wxRequest() {
 var aArguments=Array.prototype.slice.call(arguments);
 let localToken = wx.getStorageSync("localToken")||"";
 var oParams = {
   header: {
     'Content-Type': 'application/json;charset=UTF-8',
     'v':"9.2",
     'authorization': localToken
   },
 };
 //wx.setStorageSync("localToken","-=-=-=-=-=这是一个本地token-=-=-=-=-");
 //console.log("localToken---------------", localToken);
 if (aArguments.length==1){
   oParams.url = aArguments[0].url;
   oParams.method = aArguments[0].method||"POST";
   oParams.data = aArguments[0].data||{};
 }else if (aArguments.length ==2) {
   oParams.url = aArguments[0];
   oParams.method ="POST";
   oParams.data = aArguments[1]||{};
 } else if (aArguments.length == 3){
   oParams.url = aArguments[0];
   oParams.method = aArguments[1];
   oParams.data = aArguments[2]||{};
 };
  var wRequest = wxPromise(wx.request);
  return wRequest(oParams);
}

module.exports =wxRequest;
