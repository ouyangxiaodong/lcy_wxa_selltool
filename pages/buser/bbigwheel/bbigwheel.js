const wxaapi = require('../../../public/wxaapi.js');//api地址参数
const wxRequest = require('../../../utils/js/wxRequest.js'); //请求参数
const event = require('../../../public/js/wxEvent.js'); //事件上报相关参数
const wxSessionRecoder = require('../../../public/js/wxSessionRecoder.js'); //用户session记录
Page({
  data: {
    oUserInfo: {},//当前用户
    isShowMask: false, //是否显示领取成功
    lotteryNum: 0,//抽奖次数
    rules: "",//活动规则
    desUrl: "",//活动说明图片
    totalCount: 0,//总量
    aAwardsData: {},//当前大转盘对象
    aRecoders: [], //中奖记录
    aAwardsList: [], //奖品列表
    isChance: false, //
    rotateDegs: 0, //旋转角度
    aAwardsItems: [], //列表显示
    animationData: {},
    isDisabled: '', //按钮是否可用
    clinicId: '',
    sessionId:"",
    oClinic: {},//诊所信息
  },
  onLoad: function (options) {
    console.log("options",options)
    console.log("wxSessionRecoder-------------", wxSessionRecoder.fUpdateShare);
    let _This = this;
    let { sessionId, clinicId, wheelId } = options;
    clinicId = clinicId || wx.getStorageSync("clinicId") || "";
    if (clinicId) {
      wx.setStorageSync("clinicId", clinicId);
    }
    getApp().getUserData(function (oUinfo) {
      console.log("user result---------------", oUinfo);
      _This.setData({
        oUserInfo: oUinfo,
        sessionId: sessionId || "",//3218
        clinicId: clinicId || "",//3
        wheelId: wheelId || "" //15
      });
      _This.fGetBigWheel();
      _This.fGetClinicDetailByClinicId();

    });
    wx.hideShareMenu();
  },
  /**
   * 获取诊所大转盘信息
   */
  fGetBigWheel: function () {
    let _This = this;
    let pdata = {
      clinicId: _This.data.clinicId
    };
wx.showLoading({
  title: 'loading...',
});
    wxRequest(wxaapi.bigwheel.getwheel.url, pdata).then(function (result) {
       console.log("fGetBigWheel---- result---------------", result, pdata);
      if (result.data.code == 0 && result.data.data != "null") {
        let oData = result.data.data;
        console.log("oData.bigWheelGoodsList----22222-----------", oData);
        _This.setData({
          aAwardsData: oData,
          aAwardsList: oData.bigWheelGoodsList,
          lotteryNum: oData.lotteryNum,
          rules: oData.rules,
          desUrl: oData.file.url,
          wheelId: oData.id
        });
        _This.fCalculateAwards();
        return oData.id;
      } else {
        return 0;
      }
    }).then(wheelId => {
      // console.log("wheelId------------------", wheelId);
      wx.hideLoading();
      let oWheelData = {
        customerUnId: "",
        wheelId:wheelId,
        clinicId:  _This.data.clinicId,
        pageNo: 1,
        pageSize: 100,
      }
      wxRequest(wxaapi.selltool.bigWheelGoodsPageList.url, oWheelData).then(function (result) {
        // console.log("prize recoder---------------", result, oWheelData);
        if (result.data.code == 0) {
          let oDataList = result.data.data.list || [];
          _This.setData({
            aRecoders: oDataList
          });

        }
      })
    });
  },
  /**
   * 中奖奖品信息计算
   */
  fCalculateAwards: function () {
    let _This = this;
    let aAwardsList = _This.data.aAwardsList || [];
    let iAwardsLength = aAwardsList.length || 1,
      aAwardsItems = [],
      turnNum = 1 / iAwardsLength;
    let totalCount = 0;
    aAwardsList.forEach((oItem, index) => {
      // console.log("oItem------------", oItem);
      let iPrizeRate = 0;
      if (oItem.totalNum > 0) {
        iPrizeRate = oItem.prizeRate;
      }
      totalCount += iPrizeRate;
      aAwardsItems.push({
        turn: index * turnNum + 'turn',
        lineTurn: index * turnNum + turnNum / 2 + 'turn',
        award: oItem.name,
        prizeRate: iPrizeRate
      });
    });
    let random = Math.random();
    console.log("totalnum------------", totalCount, random, totalCount * random);
    // console.log("aAwardsItems-----111111111111--------------", aAwardsItems);
    _This.setData({
      aAwardsItems: aAwardsItems,
      totalCount: totalCount
    });
  },
  onReady: function (e) {

  },

  /**
    * 用户点击右上角分享
    */
  onShareAppMessage: function (e) {
    let _This = this;
    let { sessionId, wheelId, clinicId, oUserInfo } = _This.data;
    let shareurl = `/pages/bigwheel/bigwheel?sessionId=${sessionId}&wheelId=${wheelId}&clinicId=${clinicId}&cstUid=${oUserInfo.unionId}`;
    console.log("share-------------------", e, shareurl);
    return {
      title: "幸运大抽奖",
      path: shareurl,
      success: function (shareResult) {
        console.log("shareResult-------------", shareResult);
        let sType = 3;
        if (shareResult.shareTickets) {
          sType = 2;
        }
        _This.setData({
          shareType: sType,
          isShare: false,
        });


        let shareData = {
          cases: [],//案例列表Id
          consultingId: sessionId,//会话id
          consultantUnionid: oUserInfo.unionId,//咨询师unionid
          products: [],//项目列表id  [3002,3025,3028]
          type: sType // 
        };

        wxSessionRecoder.fUpdateShare(shareData);

      //  _This.fUpdateShare();
        wx.navigateToMiniProgram({
          appId: 'wx0d601009b9b6ac71', // 
          path: 'pages/index/index', //
          envVersion: wxaapi.currentEnv, //开发版 "develop",//
          success(res) {
            // 打开成功  
          }
        }) 
        // wx.redirectTo({
        //   url: '/pages/index/sharelist/home?type=share',
        // })
      }
    }

  },


  /**
   * 关闭领取奖品成功
   */
  fCloseMask() {
    let _This = this;
    _This.setData({
      isShowMask: false
    })
  },
  /**
   * 导航到奖品说明
   */
  fNavToDes() {
    let _This = this;
    let desUrl = _This.data.desUrl;
    wx.navigateTo({
      url: `/pages/bigwheel/prizedes/prizedes?desUrl=${desUrl}`,
    })
  },
  /**
   * 获取诊所信息
   */
  fGetClinicDetailByClinicId() {
    let _This = this;
    let pdata = {
      clinicId: _This.data.clinicId
    };

    wxRequest(wxaapi.clinic.getdetailbyid.url, pdata).then(function (result) {
      console.log("clinicInfo---------------", result, pdata);
      if (result.data.code == 0) {
        let oClinic = result.data.data;
        _This.setData({
          oClinic: oClinic
        });
      }
    });
  },





  ////////////////////////////////////////////////////////////////////////////////////////////////
  /**
   * 分享当前活动
   */
  fShareCurrent(e) {
    let _This = this;
    let { oUserInfo, wheelId, clinicId,sessionId}=_This.data;
    let pdata = {
      wxaOpenId: oUserInfo.openId,
      unionId: oUserInfo.unionId,
      consultationId: "",
      userLoginName: "",
      productCode: "",
      activityId: "",
      wheelId: wheelId,
      clinicId: clinicId,
      wxNickName: oUserInfo.nickName,
      consultType: 6
    };
   // console.log("pdata---share current-------", pdata);
    wxSessionRecoder.fGetSessionId(pdata,sessionId).then(isessionId=>{
      console.log("isessionId---------------", isessionId);
        _This.setData({
          isShare: true,
          isShowMsgShare: false,
          isFirstGetIn: true,
          sessionId: isessionId
        });
    });
    wx.showShareMenu({
      withShareTicket: true, //要求小程序返回分享目标信息
      success: function (res) {
         console.log("999999---------------",res);
      }
    });
  },
  /**
   * 取消分享
   */
  fCancelShare() {
    let _This = this;
    _This.setData({
      isShare: false
    });
  },
  /**
   * 分享后抽奖
   */
  fPrize(){
    wx.showToast({
      icon: 'none',
      title: '分享后抽奖',
    });
    setTimeout(()=>{
      wx.hideToast();
    },2000);
  }















  ////////////////////////////////////////////////////////////////////////////////////////////////


})