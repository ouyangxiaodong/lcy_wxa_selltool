const wxaapi = require('../../public/wxaapi.js');//api地址参数
const wxRequest = require('../../utils/js/wxRequest.js'); //请求参数
const wxPromise = require('../../utils/js/wxPromise.js');//promise信息
const event = require('../../public/js/wxEvent.js'); //事件上报相关参数
Page({

  /**
   * 页面的初始数据
   */
  data: {
    customerUnId:"",
    pageNo: 1,
    pageSize: 10,
    lastPage: false,
    list:[],
    wheelId:"",
    clinicId:"",
    nolist:false

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      customerUnId: options.customerUnId,
      clinicId: options.clinicId,
      wheelId: options.wheelId
    })
    
    this.getList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },
   
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },
  getList(){
    let _this=this;
    var oData={
      //customerUnId:'oDOgS0icXry117sZQ3q7hlVk7KSo',
      customerUnId: _this.data.customerUnId,
      clinicId: _this.data.clinicId,
      wheelId:_this.data.wheelId,
      pageNo: _this.data.pageNo,
      pageSize: _this.data.pageSize,
    }
    wx.showLoading({
      title: 'loading',
    });
    wxRequest(wxaapi.selltool.bigWheelGoodsPageList.url, oData).then(function (result) {
      if (result.data.code == 0) {
        let list = [];
        if (result.data.data.list.length == 0 ||(!result.data.data.list)){
          _this.data.nolist=true;
        }else{
          _this.data.nolist = false;
        }
        result.data.data.list.forEach((item,index)=>{
          let status="";
          if(item.status==1){
             status="未领取";
          }else if(item.status==2){
            status="已领取";
          }else if(item.status==3){
            status="已过期";
          }
          item.status=status;
          let date = _this.getdate(item.goodsCodeExpireDate);
          item.goodsCodeExpireDate=date;
          list.push(item)
  
        })
          _this.setData({
            lastPage: result.data.data.lastPage,
            list:_this.data.list.concat(list)
          })
          
          wx.hideLoading();
          
      } else {
        wx.hideLoading();
        console.log("list error---", result);
      }
    });

  },
  getdate(time){
    var date = new Date(time);
    var Y = date.getFullYear() + '.';
    var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '.';
    var D = date.getDate() < 10 ? '0' + date.getDate() : date.getDate() ; 
    return Y + M + D ;
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    let _This = this;
    let lastPage = _This.data.lastPage;
    if (!lastPage) {
      let pageNo = _This.data.pageNo;
      pageNo++;
      _This.setData({
        pageNo: pageNo
      });
      _This.getList();
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})