var aldstat = require("./utils/ald-stat.js");
const wxaapi = require('./public/wxaapi.js');
const wxRequest = require('./utils/js/wxRequest.js');
const wxPromise = require('./utils/js/wxPromise.js');
const wxCRequest = require('./public/js/crequest.js');
App({
  globalData: {
    userInfo: null,
    flag: false,
    sessionKey: ""
    // corType:"yy",
    // corBrg: "#1ED7BB"
  },
  onLaunch: function (options) {
  },
  /**
   * 获取用户登录信息
   */
 getUserData:function(callback){
   let _This=this;
   let oUserInfo = _This.globalData.userInfo||{};
   let unionId = oUserInfo.unionId;
   if (unionId){
       wxCRequest.fGetUserToken(unionId,function(tresult){
       }).then(function(tresult){
         oUserInfo.permission = tresult;
         callback(oUserInfo);
       });
     return false;
   };
   _This.fGetSessionKey(true,function (sessionKey){
     _This.fAuthUserData(sessionKey).then(resAll => {
       oUserInfo = getApp().globalData.userInfo;
       unionId = oUserInfo.unionId;
       //console.log("app.js---resAll--------------", resAll, unionId);
       if (!resAll){
         wxCRequest.fGetUserToken(unionId,function (tresult) {
         }).then(function (tresult) {
           oUserInfo.permission = tresult;
           callback(oUserInfo);
         });    
         return false;
       }
       
      wxCRequest.fGetUserToken(resAll.data.userinfo.unionId,function (tresult) {
           //callback(resAll.data.userinfo);
        }).then(function (tresult) {
           let oUserinfo=resAll.data.userinfo;
           oUserinfo.permission = tresult;
           getApp().globalData.userInfo = oUserinfo;
           callback(oUserinfo);
         });;  
       
     });
   });

 },
 /**
  * 用户授权用户信息
  */
 fAuthUserData(sessionKey){
   let _This=this;
  
  return wxPromise(wx.getUserInfo)().then(resUserInfo => {
     //console.log("----app.js------resUserInfo-----------", resUserInfo);
     if (resUserInfo.errMsg.indexOf("ok") < 0) {

     }else{
       var encryptedData = resUserInfo.encryptedData;
       var iv = resUserInfo.iv;
       var postData = {
         encryptedData: encryptedData,
         sessionKey: sessionKey, iv: iv
       };
       //return wxRequest(wxaapi.unionid.userinfo.url, postData);//解析用户信息----------------
       return wxRequest(wxaapi.sellunion.selltooluserinfo.url, postData);
     }
   })
 },
 /**
  * 获取sessionKey
  */
 fGetSessionKey: function (firstType,callback){
   let _This =this;
   let sessionKey=_This.globalData.sessionKey;
   wxPromise(wx.checkSession)().then(result => {
     if (!firstType&&result.errMsg.indexOf("ok") > 0) {
       callback(sessionKey);
     } else {
       wxPromise(wx.login)().then(result => {
         //console.log("user code------------------",result);
         let ucode = result.code;
        // return wxRequest(wxaapi.unionid.code.url, { code: ucode });//-------------------------解析用户code
         return wxRequest(wxaapi.sellunion.selltoolcode.url, { code: ucode });
       }).then(sResult => {
        // console.log("sResult session------------------", sResult);
         let resSession = sResult.data||{};
         getApp().globalData.sessionKey = resSession.session_key;
         let userInfo={
           unionId: resSession.unionid,
           openId: resSession.openid
         };
         let appUserInfo = getApp().globalData.userInfo||{};
         if (!appUserInfo.unionId){
          // appUserInfo.unionId=userInfo.unionId;
          // appUserInfo.openId = userInfo.openId;
           getApp().globalData.userInfo = userInfo;
         }
         sessionKey = resSession.session_key;
         callback(sessionKey);
       });
     }
   });
 },

})