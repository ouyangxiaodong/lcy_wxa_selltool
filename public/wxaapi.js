
// var gConfig = {
//   remoteWx: "https://node-wx.nihaomc.com",
//   remote: "https://node-wxa-selltool.nihaomc.com",
//   remoteChat: "https://node-chatapi.nihaomc.com",
//   remoteChatWss: "wss://node-chat.nihaomc.com",
//   uploadUrl: "https://api-prd.nihaomc.com/bms/attachment/upload",
//   uploadUrlV3: "https://api-prd.nihaomc.com/bms/attachment/upload/v3",
//   checkUpload:"https://api-prd.nihaomc.com/bms/api/caterial/checkUpload",
//   currentEnv:"release"
// }

var gConfig = {
  remoteWx: "https://node-wx-test.nihaomc.com",
  remote: "https://node-wxa-selltool-test.nihaomc.com",
  remoteChat: "https://node-chatapi-test.nihaomc.com",
  remoteChatWss: "wss://node-chat-test.nihaomc.com",
  uploadUrl: "https://api-uat.nihaomc.com/bms/attachment/upload",
  uploadUrlV3: "https://api-uat.nihaomc.com/bms/attachment/upload/v3",
  checkUpload:"https://api-uat.nihaomc.com/bms/api/caterial/checkUpload",
  currentEnv: "trial"
}

var urlConfig = {
  currentEnv: gConfig.currentEnv,
  img:{
    imgbase:{
      url: gConfig.remote +"/feimg"
    },
    upload:{
      url: gConfig.uploadUrl
    },
    uploadv3: {
      url: gConfig.uploadUrlV3
    },
    checkUpload:{
      url: gConfig.checkUpload
    }
  },
  wx:{
    msg: {
      sendmessage: {
        url: gConfig.remoteWx + "/msg/sendmessage"
      }
    }
  },
  user:{
    userinfo: {
      url: gConfig.remote+"/user/userinfo"  //获取用户信息
    },
    usertoken: {
      url: gConfig.remote + "/user/usertoken"//获取用户token
    },
    userresources: {
      url: gConfig.remote + "/user/userresources"//获取用户权限资源
    },
    userpermission: {
      url: gConfig.remote + "/user/userpermission"//获取用户权限资源(新)
    },
    totaltask: {
      url: gConfig.remote + "/user/totaltask"//获取用户权限资源
    }
  },
  unionid: {
    code: {
      url: gConfig.remote +"/unionid/code"
    },
    userinfo: {
      url: gConfig.remote +"/unionid/userinfo"
    }
  },
  product: {
    list: {
      url: gConfig.remote +"/product/list"
    },
    add: {
      url: gConfig.remote +"/product/add"
    }
  },
  pcase: {
    list: {
      url: gConfig.remote +"/case/list" //获取案例列表
    },
    caselistbydoctor: {
      url: gConfig.remote +"/case/caselistbydoctor" //获取医生下面的案例 new 医生名片
    },
    morelist: {
      url: gConfig.remote + "/case/morelist" //获取多案例列表
    },
    listpagebyproducts: {
      url: gConfig.remote + "/case/listpagebyproducts" //获取多案例列表带分页
    },
    detail: {
      url: gConfig.remote +"/case/detail" //项目案例详情
    },
    share: {
      url: gConfig.remote +"/case/share" //
    },
    getcaselistbysession: {
      url: gConfig.remote + "/case/getcaselistbysession" //通过sessionid获取分享案例信息
    },
  },
  customer: {
    add: {
      url: gConfig.remote + "/customer/addcustomer" //添加客户
    },
    getcustomer: {
      url: gConfig.remote + "/customer/getcustomer" //获取客户资料getcustomer
    },
    update: {
      url: gConfig.remote + "/customer/update" //更新客户
    },
    getcustomerbyunid: {
      url: gConfig.remote + "/customer/getcustomerbyunid" //通过咨询师unionid和客户unionid获取客户信息
    }
  },
  consult: {
    add: {
      url: gConfig.remote + "/consult/addconsultation" //添加会话信息
    },
    list: {
      url: gConfig.remote + "/consult/list" //获取咨询列表
    },
    trail: {
      url: gConfig.remote + "/consult/trail" //获取所有咨询轨迹
    },
    handlelike:{
      url: gConfig.remote + "/consult/handlelike"   //用户操作喜欢不喜欢  
    },
    gethandlelike: {
      url: gConfig.remote + "/consult/gethandlelike"   //获取操作喜欢不喜欢  
    },
    interactlist: {
      url: gConfig.remote + "/consult/interactlist"   //线索 互动列表
    },
    singletrail:{
      url: gConfig.remote + "/consult/singletrail" //获取单个用户咨询轨迹
    },
    consultitems: {
      url: gConfig.remote + "/consult/consultitems" //获取咨询项目
    },
    consultcustomers: {
      url: gConfig.remote + "/consult/consultcustomers" //获取一个咨询下面的所有客户
    },
    sharecase: {
      url: gConfig.remote + "/consult/sharecase" //通过会话id获取单次分享的案例ID
    },
    consultantupdate: {
      url: gConfig.remote + "/consult/consultantupdate" // 咨询会话更新接口，可更新会话与案例的关系，会话与项目的关系
    },
    entry: {
      url: gConfig.remote + "/consult/entry" //  客户进入咨询师分享的小程序，对客户信息，线索信息进行维护
    },
    cueprocessing: {
      url: gConfig.remote + "/consult/cueprocessing" //  客户进入咨询师分享的医生名片小程序，对客户信息，线索信息进行维护
    },
    getsharelike: {
      url: gConfig.remote + "/consult/getsharelike" //   查询一次分享中，单个客户对某个案例的点赞状态
    },
    handelsharecase: {
      url: gConfig.remote + "/consult/handelsharecase" // 客户进入咨询师分享的小程序，对某个案例进行点赞操作 或者进行 提交资料给医生操作  
    },
    getpostphoto: {
      url: gConfig.remote + "/consult/getpostphoto" // 获取用户上传的头像图片 
    },
    getconsultinfo: {
      url: gConfig.remote + "/consult/getconsultinfo" // 通过会话id获取咨询师信息
    },
    getcluesbyconsultid: {
      url: gConfig.remote + "/consult/getcluesbyconsultid" // 获取目标人群接口V0.3.3
    },
    addconsultrecord: {
      url: gConfig.remote + "/consult/addconsultrecord" //  保存活动目标人群接口
    },
    getrecordnum:{
      url: gConfig.remote + "/consult/getrecordnum" //  获取收礼客户
    },
    getprompt: {
      url: gConfig.remote + "/consult/getprompt" //  获取M和N
    },
    toshopgetgift: {
      url: gConfig.remote + "/consult/toshopgetgift" //  到店有礼人群接口
    }
  },
  appointment: {
    list: {
      url: gConfig.remote + "/appointment/list" //获取预约列表
    },
    detail: {
      url: gConfig.remote + "/appointment/detail" //获取预约详情
    },
    send: {
      url: gConfig.remote + "/appointment/send" //发起预约
    }
  },
  event: {
    add: {
      url: gConfig.remote +"/event/add"   //分享访问相关接口添加事件
    },
    v2: {
      url: gConfig.remote + "/event/v2"   //v2添加预约相关接口事件
    }
  },
  clue: {
    detail: {
      url: gConfig.remote + "/clue/detail"   //获取线索详情
    },
    toshop: {
      url: gConfig.remote + "/clue/toshop"   //现场咨询师更新线索状态为已到店
    },
    pageList: {
      url: gConfig.remote + "/clue/pageList"   //已到店 线索列表
    },
    promptlist: {
      url: gConfig.remote + "/clue/promptlist"   //已到店 线索列表
    },
    getcluecasebyunid: {
      url: gConfig.remote + "/clue/getcluecasebyunid"
    }
  },
  clinic: {
    detail: {
      url: gConfig.remote + "/clinic/detail"   //获取诊所详情
    },
    getdetailbyid: {
      url: gConfig.remote + "/clinic/getdetailbyid" //通过诊所id 获取诊所详情
    },
    getuseablecliniclist: {
      url: gConfig.remote + "/clinic/getuseablecliniclist" //获取当前已经存在的诊所信息
    }
  },
  index:{
    cluelist: {
      url: gConfig.remote + "/index/cluelist"   //获取线索列表
    },
    cluedetail: {
      url: gConfig.remote + "/index/cluedetail"   //获取线索详情
    },
    
    sharelist: {
      url: gConfig.remote + "/index/sharelist"   //场景列表
    },
    clueremark: {
      url: gConfig.remote + "/index/clueremark"   //线索备注
    },
    remarklist: {
      url: gConfig.remote + "/index/remarklist"   //备注列表
    },
   
    clueclose: {
      url: gConfig.remote + "/index/clueclose"   //线索关闭
    },
    linkmanupdate: {
      url: gConfig.remote + "/index/linkmanupdate"   //联系人更新
    },
    linkman: {
      url: gConfig.remote + "/index/linkman"   //联系人信息
    },
    waitflow: {
      url: gConfig.remote + "/index/waitflow"   //待跟进
    }
  },
  posterinfo:{
    addposter:{
      url: gConfig.remote + "/posterinfo/addposter"   //新建海报
    }, 
    pagelist: {
      url: gConfig.remote + "/posterinfo/pagelist"   //海报列表查询
    },
    posterdel: {
      url: gConfig.remote + "/posterinfo/posterdel"   //海报删除
    },
    createposter:{
      url: gConfig.remote + "/api/createposter"//创建保存海报图片
    },
    deleteposter:{
      url: gConfig.remote + "/api/deleteposter"//删除生成的海报图片 
    }
  },
  postercategory: {
    addorupdate: {
      url: gConfig.remote + "/postercategory/addorupdate"   //添加、修改海报分类
    }, list: {
      url: gConfig.remote + "/postercategory/list"   //分类列表查询
    }
  },
  wxaqr:{
    genwxaqrcode:{
      url: gConfig.remote + "/wxaqr/genwxaqrcode"
    },
    addformid:{
      url: gConfig.remote + "/wxa/formid"
    },
    gConfig:{
      route: gConfig.remote
    }
  },
  gift:{
    pagelist:{
      url: gConfig.remote + "/gift/pagelist"  //获取礼品列表 
    },
    giftdetail: {
      url: gConfig.remote + "/gift/giftdetail"  //获取礼品详情 giftdetail
    }
  },
  activityrecord: {
    create: {
      url: gConfig.remote + "/activityrecord/create"  //保存领取记录 
    },
    getnum: {
      url: gConfig.remote + "/activityrecord/getnum"  //领取总数 
    },
    pagelist: {
      url: gConfig.remote + "/activityrecord/pagelist"  // 获取列表 
    },
    getdetail: {
      url: gConfig.remote + "/activityrecord/getdetail"  //获取领取详情 
    },
    getalreadyappointmentnum: {
      url: gConfig.remote + "/activityrecord/getalreadyappointmentnum"  // 已经领取总数  
    },
    getbubbleprompt: {
      url: gConfig.remote + "/activityrecord/getbubbleprompt"  // 气泡  
    },
    couponpagelist: {
      url: gConfig.remote + "/activityrecord/couponpagelist"  //客户优惠券列表  
    },
    couponverification: {
      url: gConfig.remote + "/activityrecord/couponverification"  //优惠券核销接口 
    }
  },
  collect:{
    createtag:{
      url: gConfig.remote +'/collect/createtag'
    },
    taglist: {
      url: gConfig.remote + '/collect/taglist'
    },
    customerlist: {
      url: gConfig.remote + '/collect/customerlist'
    },
    doctorlist: {
      url: gConfig.remote + '/collect/doctorlist'
    },
    productlist: {
      url: gConfig.remote + '/collect/productlist'
    },
    createcustomer: {
      url: gConfig.remote + '/collect/createcustomer'
    },
    list: {
      url: gConfig.remote + '/collect/list'
    },
    create: {
      url: gConfig.remote + '/collect/create'
    },
    recreate: {
      url: gConfig.remote + '/collect/recreate'
    },
    detail: {
      url: gConfig.remote + '/collect/detail'
    },
    getthubm: {
      url: gConfig.remote + '/collect/getthubm'
    }
  },
  doctor: {
    getbyid: {
      url: gConfig.remote + "/doctor/getbyid"   //获取医生详情
    },
    isopendoctor: {
      url: gConfig.remote + "/doctor/isopendoctor"   //该诊所是否允许显示医生
    },
    doctorsession: {
      url: gConfig.remote + "/doctor/doctorsession"   //根据sessionid获取医生详情
    }
  },
  coupon:{ //优惠券
    pagelist:{
      url: gConfig.remote + "/coupon/pagelist"   //获取优惠券列表，带分页
    }
  },
  mediabase: { // 案例照片/视频收集相关接口
    pagelist: {
      url: gConfig.remote + "/mediabase/pagelist"   // 获取用户上传案例列表
    }
  },
  statistics:{
    casestatistics: {
      url: gConfig.remote + "/statistics/casestatistics"   //一次分享获得客户统计
    },
    booklist:{
      url: gConfig.remote + "/statistics/booklist"   //已经预约未预约列表
    },
    viewlist: {
      url: gConfig.remote + "/statistics/viewlist"   //已经预约未预约列表
    },
    potentiallist: {
      url: gConfig.remote + "/statistics/potentiallist"   //已经预约未预约列表
    }
  
  },
  triage: { // 分诊接口
    wxlist: {
      url: gConfig.remote + "/triage/wxlist"   // 获取用户上传案例列表
    }
  },
  newarrivals: { // 新上架列表接口
    newarrivalslist: {
      url: gConfig.remote + "/newarrivals/newarrivalslist"  
    }
  },
  useroperate: { //b端进入小程序操作营销物料红点
    create: {
      url: gConfig.remote + "/useroperate/create"
    },
    update: {
      url: gConfig.remote + "/useroperate/update"
    },
    updateoperatestatus: {
      url: gConfig.remote + "/useroperate/updateoperatestatus"
    }
  },
  image: { // 术前术后图片合成
    synthesis: {
      url: gConfig.remote + "/image/synthesis"
    }
  },
  statisticsclinic: { // 热门统计
    hotproductlist: {
      url: gConfig.remote + "/statisticsclinic/hotproductlist" //热门项目
    },
    hotcliniclist: {
      url: gConfig.remote + "/statisticsclinic/hotcliniclist" //热门诊所
    },
    hotdoctorlist: {
      url: gConfig.remote + "/statisticsclinic/hotdoctorlist" //热门医生
    }
  },
  activitycustomer: { // 活动客户
    create: {
      url: gConfig.remote + "/activitycustomer/create" //新增客户
    },
    update: {
      url: gConfig.remote + "/activitycustomer/update" //更新客户
    },
    getcustomer: {
      url: gConfig.remote + "/activitycustomer/get" //获取客户信息
    }
  },
  activitysource: { // 活动来源
    create: {
      url: gConfig.remote + "/activitysource/create" //创建来源
    }
  },
  activityinfo: { // 品团活动
    getgroupinfo: {
      url: gConfig.remote + "/activityinfo/getgroupinfo" //团详情、团列表 
    },
    getClinicList: {
      url: gConfig.remote + "/activityinfo/getClinicList" //九宫格
    },
    getgroupList: {
      url: gConfig.remote + "/activityinfo/getgroupList" //拼团列表
    },

    isShowGroup:{
      url: gConfig.remote + "/activityinfo/isShowGroup"
    },
    findactivitylist: {
      url: gConfig.remote + "/activityinfo/findactivitylist" //根据诊所id获取活动列表
    },
    list: {
      url: gConfig.remote + "/activityinfo/list" //获取诊所单个活动
    },
    getgroupinfogromcase: {
      url: gConfig.remote + "/activityinfo/getgroupinfogromcase" //获取用户参团信息
    },
    findactivitylistpagebyapp: {
      url: gConfig.remote + "/activityinfo/findactivitylistpagebyapp" //C端小程序活动列表
    },
    getactivityinfobysession: {
      url: gConfig.remote + "/activityinfo/getactivityinfobysession" //根据sessionID获取活动详情
    }
  },
  sms:{
    sendcode: {
      url: gConfig.remote + "/sms/sendcode" //发送短信验证码
    },
    checkcode: {
      url: gConfig.remote + "/sms/checkcode" //校验短信验证码 
    }
  },
  chat:{
    socketurl:{
      url:gConfig.remoteChatWss
    },
    createUser: {
      url: gConfig.remoteChat + "/users" 
    }, 
    relation: {
      url: gConfig.remote + "/chat/relation"
    },
    history: {
      url: gConfig.remoteChat + "/users/history"
    },
    historycount:{
      url: gConfig.remoteChat + "/users/historycount",
      method:"GET"
    },
    messages:{
      url: gConfig.remoteChat + "/messages",
      method: "GET"
    }
  },
  payment: { // 微信支付
    orderpay: {
      url: gConfig.remote + "/payment/orderpay" //小程序支付
    }
  },
  /////////////////////////////////////////////////////////
  selltool:{//
    bigWheelGoodsPageList:{
      url: gConfig.remote +"/selltool/bigWheelGoodsPageList"//奖品列表
    },
    createbigwheel:{
      url: gConfig.remote +"/selltool/createbigwheel"//领取奖品
    }
  },
  bigwheel:{//
    getwheel:{
      url: gConfig.remote +"/bigwheel/getwheel"//获取大转盘信息
    }
  },
  sellunion:{ //新的营销工具解析用户信息
    selltoolcode: {
      url: gConfig.remote + "/sellunion/selltoolcode"
    },
    selltooluserinfo: {
      url: gConfig.remote + "/sellunion/selltooluserinfo"
    }
  }

}
module.exports = urlConfig;