
const wxRequest = require('../../utils/js/wxRequest.js');
const wxaapi = require('../wxaapi.js');
function fGetSessionId(oPdata,isessionId) {
  let _This = this;
  if (isessionId){
    return new Promise((resolve) => {
      resolve(isessionId);
    });
  }
  // let pdata = {
  //   wxaOpenId: oUserInfo.openId,
  //   unionId: oUserInfo.unionId,
  //   consultationId: "",
  //   userLoginName: "",
  //   productCode: "",
  //   activityId: activityId,
  //   clinicId: clinicId,
  //   wxNickName: oUserInfo.nickName,
  //   consultType: 5
  // };
  return wxRequest(wxaapi.consult.add.url, oPdata).then(function (result) {
    console.log("fgetsessionid--------------", oPdata, result);
    if (result.data.code == 0) {
      // _This.setData({
      //   sessionId: result.data.data
      // });
      let sessionId=result.data.data||"";
      return sessionId;
    } else {
      console.log("fgetsessionid--------- error==", result);
      return "";
    }
  });
}
function fUpdateShare(oShareData) {
  let _This = this;
  // let { sessionId, oUserInfo, shareType } = _This.data;
  // let shareData = {
  //   cases: [],//案例列表Id
  //   consultingId: sessionId,//会话id
  //   consultantUnionid: oUserInfo.unionId,//咨询师unionid
  //   products: [],//项目列表id  [3002,3025,3028]
  //   type: shareType // 
  // };
  wxRequest(wxaapi.consult.consultantupdate.url, oShareData).then(function (result) {
    console.log("update share-----------", oShareData,result);
    if (result.data.code == 0) {
      // _This.fUserEvent(event.eType.appShare);
    } else {
      console.log(result);
    }
    wx.hideLoading();
  });
}

module.exports = {
  fGetSessionId: fGetSessionId,
  fUpdateShare: fUpdateShare
};