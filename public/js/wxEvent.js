const wxRequest = require('../../utils/js/wxRequest.js');
const wxaapi = require('../wxaapi.js');
var eType = {
  appShare: "appShare", //咨询师分享
  appOpen: "appOpen",//打开小程序
  openDoctor:"openDoctor",//打开分享医生
  openActivity:"openActivity",//打开分享活动
  caseLike: "caseLike",//喜欢不喜欢案例
  photoUpload: "photoUpload",//照片上传
  informationSubmit: "informationSubmit", //信息提交
  appQuit: "appQuit", //退出程序+
  noteAdd:"noteAdd",//添加备注+
  reserve: "reserve",//预约+
  contactEdit: "contactEdit",//编辑联系人+
  leadClose: "leadClose",//关闭联系人线索+
  authPhone: "authPhone",//授权手机号+
  giftSend:"giftSend",//群发有礼
  openGift:"openGift",//浏览礼品
  getGift:"getGift", //领取礼品
  getCoupon: "getCoupon",//领取优惠券
  viewCase:"viewCase",//查看案例横向20180512
  viewCaseLog: "viewCaseLog",//竖向查看日志20180512
  playVideo: "playVideo",//案例播放视频20180512
  clickToChat: "clickToChat",//导航到客服会话
  seeInformation: "seeInformation",//查看诊所信息20180817
  telephoneConsultation: "telephoneConsultation",// 电话咨询20180817
  openGroup: "openGroup",// 立即开团20180817
  submitOrder:"submitOrder",//提交订单
  viewMap:"viewMap",//查看诊所地图
  luckDraw: "luckDraw",//点击抽奖20180907
  winningThePrize: "winningThePrize",//中奖20180907
  lookAtThePrize: "lookAtThePrize",//查看我的奖品20180907
  receivePrize: "receivePrize",//去领取20180907
  openWheel: "openWheel",//打开大转盘
};
var oEvent = {
  shareEventId: "", //当事件码是 appShare，获取该值，传值的时候带着相关的参数
  code: "", //事件码eType
  shareEventId:"",
  productCode:"",
  clueId:"", //线索id
  consultationId:"",//咨询会话ID
  sceneId:"",//场景
  eventAttrs: {
    wheelId:"",
    doctorId: "",//医生id
    activityId: "",//活动id
    appletId: "hldn",
    consultingId: 0,
    consultantId: "",
    triggeredTime: "",
    case: "",
    isLike: "",
    image: ""

  },
  subjectAttrs: {
    appid: "yxy",
    consultantId: "",
    openid: "",
    unionid: "",
    mobile: ""
  }
}
var fEvent=function(){

}
module.exports = {
  eType: eType,
  oEvent: oEvent,
  fEvent: fEvent
};