/**
 * 根据诊所类型设置机构颜色
 */
function setTypeColor(industryType){
  let oColor={
    corType:"mk",
    corBrg:"#9083ed"
  }
  if(industryType==2){
    oColor = {
      corType: "yy",
      corBrg: "#1BC3B8"
    }
  }
  wx.setStorageSync("corType", oColor.corType);
  wx.setStorageSync("corBrg", oColor.corBrg);
  return oColor;
}
module.exports = {
  setTypeColor, setTypeColor
}