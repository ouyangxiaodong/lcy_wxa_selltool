const wxRequest = require('../../utils/js/wxRequest.js');
const wxaapi = require('../wxaapi.js');

var fGetBunionId = function (customerUnionId, clinicId, userId){

  let retUnionid ="";
  var chatData = {
    consultorId: userId||"",
    customerUnionId: customerUnionId,
    clinicId: clinicId
  }
  // var chatData = {
  //   consultorId: 322,
  //   customerUnionId: "oDOgS0p-KRILoEhsXIYxTeUsGdSg",
  //   clinicId: 152
  // }
  console.log("客服信息参数-------pdata-----",chatData);
  return wxRequest(wxaapi.chat.relation.url, chatData).then(function (result) {
    console.log("客服信息-----------------",result);
    if (result.data.code == 0) {
      var data = result.data.data;
      var pdata = {
        nickName: data.wxNickname,
        gender: data.gender,
        language: data.language,
        city: data.city,
        province: data.province,
        country: data.country,
        avatarUrl: data.avatarUrl,
        unionid: data.unionId,
        openid: data.openId,
        userid: data.userId,
        username: data.userName
      }

      retUnionid = data.unionId;
     return  wxRequest(wxaapi.chat.createUser.url, pdata)
    }else{
      return {data:{}};
    }
  }).then(function (result) {
    if (result.data.code == 200) {
      console.log("---创建用户----", retUnionid);
      return retUnionid;
    }else{
      console.log("---创建用户--失败--");
      return "";
    }
  })
}


let fGetUserInfo=function(oUserInfo){
  //console.log(resSession)
  let pdata = {
    nickName: oUserInfo.nickName|| "游客",
    gender: oUserInfo.gender||2,
    language: oUserInfo.language,
    city: oUserInfo.city,
    province: oUserInfo.province,
    country: oUserInfo.country,
    avatarUrl: oUserInfo.avatarUrl||"https://mk-node-wxa.nihaomc.com/feimg/img-demo.png",
    unionid: oUserInfo.unionId,
    openid: oUserInfo.openId,
    userid: "",
    username: ""
  }
  return wxRequest(wxaapi.chat.createUser.url, pdata).then(function (result) {
    //console.log("------uchat-创建用户--result--", result)
    if (result.data.code == 200) {
      //console.log("---创建用户----", result)
    }
    return {};
  })
}

module.exports = {
  fGetBunionId,fGetUserInfo
}